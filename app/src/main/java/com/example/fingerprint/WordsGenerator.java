package com.example.fingerprint;

import java.security.InvalidParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static com.example.fingerprint.Utils.BinToHex;
import static com.example.fingerprint.Utils.bytetohex;
import static com.example.fingerprint.Utils.generateMD5;
import static com.example.fingerprint.Utils.generateSHA256;
import static com.example.fingerprint.Utils.hexStringToByteArray;
import static com.example.fingerprint.Utils.hexToBin;
import static com.example.fingerprint.Utils.splitToNChar;

public class WordsGenerator {

    private static List<String> list=new ArrayList<>();
    private static HashMap<Integer, String> hmap = new HashMap<>();
    public static String entropyInBinary;
    public static  String[] checksum;
    public static    int[] decimal;

    static List<String>  Wordsgenerator(int WordsNumber,String finger)
            throws NoSuchAlgorithmException, InvalidParameterException {

        byte[] edr=finger.getBytes();


        if (WordsNumber==24){

            entropyInBinary = hexToBin(bytetohex(edr));

            //2-generate checkSum
            String  sha256ofEntropy = hexToBin(bytetohex(generateSHA256(edr)));
            checksum = splitToNChar(sha256ofEntropy, 8);

        }else if(WordsNumber==12){

            String entbin=hexToBin(bytetohex(edr));
            byte[] entropy=hexStringToByteArray(BinToHex(entbin));


            entropyInBinary = hexToBin(bytetohex(generateMD5(entropy)));

            //2-generate checkSum
            String sha256ofEntropy = hexToBin(bytetohex(generateSHA256(edr)));
            checksum = splitToNChar(sha256ofEntropy, 4);
            
        }

        //3-Calculate Seed(append checksum to entropy)
        String entopyWITHchecksum = entropyInBinary + checksum[0];
     

        //4-divide seed into 12 parts that each part have 11 bits
        String[] split = splitToNChar(entopyWITHchecksum, 11);

        //5-convert every part to deciamal number between 0 to 2047
        decimal = new int[(entopyWITHchecksum.length()) / 11];
        for (int i = 0; i < decimal.length; i++) {

            decimal[i] = Integer.parseInt(split[i], 2);

        }

        //6-assign a word from Wordlist to each decimal number
        for (int value : decimal) {

            hmap.put(value, EnglishWordList.words[value]);
            list.add(EnglishWordList.words[value]);
            Log.e("edriswords",EnglishWordList.words[value]);

        }

        return list;
    }
}
