package com.example.fingerprint;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.security.KeyStore;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_NAME = "yourKey";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    Button CreateWallet;
    TextView Errortxt,TouchTxt;
    ImageView fingerImg;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CreateWallet = findViewById(R.id.create_wallet);
        Errortxt = findViewById(R.id.error_txt);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            keyguardManager =
                    (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            fingerprintManager =
                    (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

            //Check whether the device has a fingerprint sensor//

            if (fingerprintManager != null) {
                if (!fingerprintManager.isHardwareDetected()) {
                    // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                    CreateWallet.setVisibility(View.GONE);
                    Errortxt.setText("fingerprint sensor not exist in your phone");

                }
                //Check whether the user has granted your app the USE_FINGERPRINT permission//
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    // If your app doesn't have this permission, then display the following text//
                    CreateWallet.setVisibility(View.GONE);
                    Errortxt.setText("allow app use fingerprint");
                }

                //Check that the user has registered at least one fingerprint//
                if (!fingerprintManager.hasEnrolledFingerprints()) {
                    // If the user hasn’t configured any fingerprints, then display the following message//
                    CreateWallet.setVisibility(View.GONE);
                    Errortxt.setText("configure your fingerprint in your phone");

                }

                //Check that the lockscreen is secured//
                if (!keyguardManager.isKeyguardSecure()) {
                    // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                    CreateWallet.setVisibility(View.GONE);
                    Errortxt.setText("secure your phone with a pin password or pattern");

                } else {

                    CreateWallet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(MainActivity.this);

                            LayoutInflater inflater = getLayoutInflater();
                            View view1 = inflater.inflate(R.layout.fingerview, null);
                            fingerImg=view1.findViewById(R.id.finger_img);
                            progressBar=view1.findViewById(R.id.progressbar);
                            TouchTxt=view1.findViewById(R.id.touch_txt);
                            builder.setView(view1);
                            builder.setCancelable(false);
                            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });

                            builder.create();
                            builder.show();
                            try {
                                generateKey();
                            } catch (FingerprintException e) {
                                e.printStackTrace();
                            }
                            if (initCipher()) {
                                //If the cipher is initialized successfully, then create a CryptoObject instance//
                                cryptoObject = new FingerprintManager.CryptoObject(cipher);

                                // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                                // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                                FingerPrintHandler helper = new FingerPrintHandler(MainActivity.this);

                                helper.startAuth(fingerprintManager, cryptoObject);


                            }

                        }
                    });

                }
            }

        }

    }

    private static class FingerprintException extends Exception {
        public FingerprintException(Exception e) {
            super(e);

        }
    }

    private void generateKey() throws FingerprintException {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                keyGenerator.init(new

                        //Specify the operation(s) this key can be used for//
                        KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_ENCRYPT |
                                KeyProperties.PURPOSE_DECRYPT)
                        .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                        //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                        .setUserAuthenticationRequired(true)
                        .setEncryptionPaddings(
                                KeyProperties.ENCRYPTION_PADDING_PKCS7)
                        .build());
            }

            //Generate the key//
            keyGenerator.generateKey();


        } catch (Exception exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean initCipher() throws RuntimeException {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (Exception e) {

            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);

            //Return true if the cipher has been initialized successfully//
            return true;
        } catch (Exception e) {

            //Return false if cipher initialization failed//
            return false;
        }


    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public class FingerPrintHandler extends FingerprintManager.AuthenticationCallback {

        private CancellationSignal cancellationSignal;
        private Activity activity;
        Handler handler;

        private static final String KEY_NAME = "yourKey";
        private Cipher cipher;
        private KeyStore keyStore;
        private KeyGenerator keyGenerator;
         String xx;

        public FingerPrintHandler(Activity activity) {
            this.activity = activity;
        }

        //Implement the startAuth method, which is responsible for starting the fingerprint authentication process//

        public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                cancellationSignal = new CancellationSignal();
            }
            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
            }
        }

        @Override
        //onAuthenticationError is called when a fatal error has occurred. It provides the error code and error message as its parameters//

        public void onAuthenticationError(int errMsgId, CharSequence errString) {
            Toast.makeText(activity, "خطا در تایید", Toast.LENGTH_SHORT).show();
        }

        @Override
        //onAuthenticationFailed is called when the fingerprint doesn’t match with any of the fingerprints registered on the device//
        public void onAuthenticationFailed() {
            Toast.makeText(activity, "تایید ناموفق", Toast.LENGTH_SHORT).show();
        }

        @Override
        //onAuthenticationHelp is called when a non-fatal error has occurred. This method provides additional information about the error,
        //so to provide the user with as much feedback as possible I’m incorporating this information into my toast//
        public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
            Toast.makeText(activity, helpString, Toast.LENGTH_LONG).show();
        }

        @Override

        //onAuthenticationSucceeded is called when a fingerprint has been successfully matched to one of the fingerprints stored on the user’s device//
        public void onAuthenticationSucceeded(
                FingerprintManager.AuthenticationResult result) {
            fingerImg.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            TouchTxt.setVisibility(View.GONE);

            try {
                generateKey();
                initCipher();
            } catch (FingerprintException e) {
                e.printStackTrace();
            }


            handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                Intent intent=new Intent(activity, InformationActivity.class);
                intent.putExtra("finger",xx);
                activity.startActivity(intent);

                activity.finish();

            }
        }, 4000);

        }

        private class FingerprintException extends Exception {
            public FingerprintException(Exception e) {
                super(e);

            }
        }

        private void generateKey() throws FingerprintException {
            try {
                // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
                keyStore = KeyStore.getInstance("AndroidKeyStore");

                //Generate the key//
                keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

                //Initialize an empty KeyStore//
                keyStore.load(null);

                //Initialize the KeyGenerator//
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    keyGenerator.init(new

                            //Specify the operation(s) this key can be used for//
                            KeyGenParameterSpec.Builder(KEY_NAME,
                            KeyProperties.PURPOSE_ENCRYPT |
                                    KeyProperties.PURPOSE_DECRYPT)
                            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                            //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                            .setUserAuthenticationRequired(true)
                            .setEncryptionPaddings(
                                    KeyProperties.ENCRYPTION_PADDING_PKCS7)
                            .build());
                }

                //Generate the key//
                keyGenerator.generateKey();


            } catch (Exception exc) {
                exc.printStackTrace();
                throw new FingerprintException(exc);
            }
        }

        //Create a new method that we’ll use to initialize our cipher//
        @RequiresApi(api = Build.VERSION_CODES.M)
        public boolean initCipher() throws RuntimeException {
            try {
                //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
                cipher = Cipher.getInstance(
                        KeyProperties.KEY_ALGORITHM_AES + "/"
                                + KeyProperties.BLOCK_MODE_CBC + "/"
                                + KeyProperties.ENCRYPTION_PADDING_PKCS7);
            } catch (Exception e) {

                throw new RuntimeException("Failed to get Cipher", e);
            }

            try {
                keyStore.load(null);
                SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                        null);
                cipher.init(Cipher.ENCRYPT_MODE, key);

                xx = key.toString().substring(key.toString().lastIndexOf("@") + 1);

                //Return true if the cipher has been initialized successfully//
                return true;
            } catch (Exception e) {

                //Return false if cipher initialization failed//
                return false;
            }

        }
    }
}