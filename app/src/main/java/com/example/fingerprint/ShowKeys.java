package com.example.fingerprint;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import static com.example.fingerprint.Utils.AddressGenerator;
import static com.example.fingerprint.Utils.GeneratePrivateKey;
import static com.example.fingerprint.Utils.GeneratePublicKey;
import static com.example.fingerprint.Utils.PBKDF2;
import static com.example.fingerprint.Utils.bytetohex;
import static com.example.fingerprint.Utils.hexStringToByteArray;
import static com.example.fingerprint.Utils.hexToBin;

public class ShowKeys extends AppCompatActivity {
    TextView privatekey,publickey,address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_keys);
        privatekey=findViewById(R.id.privatekey);
        publickey=findViewById(R.id.publickey);
        address=findViewById(R.id.address);
        final Intent intent=getIntent();
          String wordstogether=intent.getStringExtra("wordstogether");

        try {
            byte[] Seed = PBKDF2(wordstogether, "");
           
            String PrivateKey= GeneratePrivateKey(Seed);
           

            byte[] PublicKeyInByte = GeneratePublicKey(hexStringToByteArray(PrivateKey), true);
            String PublicKey=bytetohex(PublicKeyInByte);
           
            String Address=AddressGenerator(PublicKeyInByte);
           

            privatekey.setText("private key : "+PrivateKey);
            publickey.setText("public key : "+PublicKey);
            address.setText("address : "+Address);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }


    }
}