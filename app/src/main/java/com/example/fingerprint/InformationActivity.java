package com.example.fingerprint;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import static com.example.fingerprint.Utils.ConcateWords;

public class InformationActivity extends AppCompatActivity {
    TextView word1,word2,word3,word4,word5,word6,
            word7,word8,word9,word10,word11,word12;

    Button showkeys;
    String wordsTogether;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        word1=findViewById(R.id.word1);
        word2=findViewById(R.id.word2);
        word3=findViewById(R.id.word3);
        word4=findViewById(R.id.word4);
        word5=findViewById(R.id.word5);
        word6=findViewById(R.id.word6);
        word7=findViewById(R.id.word7);
        word8=findViewById(R.id.word8);
        word9=findViewById(R.id.word9);
        word10=findViewById(R.id.word10);
        word11=findViewById(R.id.word11);
        word12=findViewById(R.id.word12);
        showkeys=findViewById(R.id.submit);
        final Intent intent=getIntent();
       String finger=intent.getStringExtra("finger");

        try {
            List<String> words=WordsGenerator.Wordsgenerator(12,finger);
           word1.setText(words.get(0));
           word2.setText(words.get(1));
           word3.setText(words.get(2));
           word4.setText(words.get(3));
           word5.setText(words.get(4));
           word6.setText(words.get(5));
           word7.setText(words.get(6));
           word8.setText(words.get(7));
           word9.setText(words.get(8));
           word10.setText(words.get(9));
           word11.setText(words.get(10));
           word12.setText(words.get(11));
           wordsTogether = ConcateWords(words);

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        showkeys.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1=new Intent(InformationActivity.this,ShowKeys.class);
                intent1.putExtra("wordstogether",wordsTogether);
                startActivity(intent1);
            }
        });

    }
}