package com.example.fingerprint;

import org.bouncycastle.crypto.digests.RIPEMD160Digest;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Utils {
    static String bytetohex(byte[] byteArray) {
        final String HEXADECIMALS = "0123456789abcdef";

        final StringBuilder hexText = new StringBuilder(2 * byteArray.length);

        for (byte byteElement : byteArray) {
            hexText
                    .append(HEXADECIMALS.charAt((byteElement & 0xF0) >> 4))
                    .append(HEXADECIMALS.charAt((byteElement & 0x0F)));
        }

        return hexText.toString();
    }

    static String hexToBin(String s) {

        String value = new BigInteger(s, 16).toString(2);
        String formatPad = "%" + (s.length() * 4) + "s";
        return String.format(formatPad, value).replace(" ", "0");
    }


    static String ConcateWords(List<String> wordList) {

        StringBuilder sb = new StringBuilder();
        for (String s : wordList) {
            sb.append(s).append(' ');
        }
        return sb.toString().trim();
    }

    static String[] splitToNChar(String text, int size) {
        List<String> parts = new ArrayList<>();

        int length = text.length();
        for (int i = 0; i < length; i += size) {
            parts.add(text.substring(i, Math.min(length, i + size)));
        }
        return parts.toArray(new String[0]);
    }

    static String BinToHex(String binary) {

        return new BigInteger(binary, 2).toString(16);
    }

    static String Base58Encode(byte[] input) {
        final String ALPHABET = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
        final BigInteger BASE = BigInteger.valueOf(58);

        BigInteger bi = new BigInteger(1, input);
        StringBuilder s = new StringBuilder();
        while (bi.compareTo(BASE) >= 0) {
            BigInteger mod = bi.mod(BASE);
            s.insert(0, ALPHABET.charAt(mod.intValue()));
            bi = bi.subtract(mod).divide(BASE);
        }
        s.insert(0, ALPHABET.charAt(bi.intValue()));

        for (byte anInput : input) {
            if (anInput == 0)
                s.insert(0, ALPHABET.charAt(0));
            else
                break;
        }
        return s.toString();
    }


    static String RIPEMD160Algorithm(byte[] hash)  {

        RIPEMD160Digest d = new RIPEMD160Digest();
        d.update (hash, 0, hash.length);
        byte[] o = new byte[d.getDigestSize()];
        d.doFinal (o, 0);

        return bytetohex(o);

    }

    static byte[] generateSHA256(byte[] textToHash)
            throws NoSuchAlgorithmException {

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(textToHash);

        return messageDigest.digest();
    }
    static byte[] generateMD5(byte[] textToHash)
            throws NoSuchAlgorithmException {

        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(textToHash);

        return messageDigest.digest();
    }

    static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private static byte[] combine(final byte[] array1, final byte[] array2) {
        final byte[] bytes = new byte[array1.length + array2.length];
        System.arraycopy(array1, 0, bytes, 0, array1.length);
        System.arraycopy(array2, 0, bytes, array1.length, bytes.length - array1.length);
        return bytes;
    }

    static byte[] PBKDF2(String mnemonic, String passphrase)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        String fixedSalt="mnemonic";
        final byte[] fixedSaltByte = fixedSalt.getBytes(StandardCharsets.UTF_8);

        final char[] normalizeMnemonic = Normalizer.normalize(mnemonic,Normalizer.Form.NFKD).toCharArray();
        final String normalizedPassphrase = Normalizer.normalize(passphrase,Normalizer.Form.NFKD);
        final byte[] VariableSalt = normalizedPassphrase.getBytes(StandardCharsets.UTF_8);
        final byte[] salt = combine(fixedSaltByte, VariableSalt);
        int iterations=2048;
        int keylength=512;

        PBEKeySpec spec = new PBEKeySpec(normalizeMnemonic, salt, iterations,keylength);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        return skf.generateSecret(spec).getEncoded();
    }


    static String GeneratePrivateKey(byte[] value){
        String key="Bitcoin seed";
        try {
            SecretKeySpec keySpec = new SecretKeySpec(
                    key.getBytes(StandardCharsets.UTF_8),
                    "HmacSHA512");

            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(keySpec);
            String macbyte=Utils.bytetohex(mac.doFinal(value));
            String macBinary = hexToBin(macbyte);
            System.out.print("\nneed :"+macbyte);
            String[] binarymac = splitToNChar(macBinary, 256);
            String PrivateKey = BinToHex(binarymac[0]);
            String chainCode = BinToHex(binarymac[1]);
            return PrivateKey;

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    static byte[] GeneratePublicKey(byte[] privateKey, boolean compressed) {

        ECNamedCurveParameterSpec spec = ECNamedCurveTable.getParameterSpec("secp256k1");
        ECPoint pointQ = spec.getG().multiply(new BigInteger(1, privateKey));
        return pointQ.getEncoded(compressed);
    }

    static String AddressGenerator(byte[] pubKey) throws NoSuchAlgorithmException {

        //referece :  https://en.bitcoin.it/wiki/Technical_background_of_version_1_Bitcoin_addresses

        //step-1
        byte[] EncodeOfPublicKey = generateSHA256(pubKey);

        //step-2
        String RimpemdOfPublicKey = RIPEMD160Algorithm(EncodeOfPublicKey);

        //step-3
        String MainNetPrefix="00"+ RimpemdOfPublicKey;

        //step-4
        byte[] encodedhash1 = generateSHA256(hexStringToByteArray(MainNetPrefix));

        //step-5
        byte[] encodedhash2 = generateSHA256(encodedhash1);

        //step-6
        String[] checksumAdrress=splitToNChar(bytetohex(encodedhash2),8);
        String AddressChecksum=checksumAdrress[0];


        //step-7
        String ConcatingChecksum= MainNetPrefix +AddressChecksum;

        //step-8
        String Address=Base58Encode(hexStringToByteArray(ConcatingChecksum));

        return Address;

    }

}
